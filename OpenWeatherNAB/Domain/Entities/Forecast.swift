//
//  Forecast.swift
//  OpenWeatherNAB
//
//  Created by Luong Nghia on 6/21/20.
//  Copyright © 2020 Luong Nghia. All rights reserved.
//

import Foundation

struct Forecast {
    let date: Int
    let avgTempature: Double
    let pressure: Int
    let humidity: Int
    let description: String
}
